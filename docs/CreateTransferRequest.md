# Mobwa.CreateTransferRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**message** | **String** |  | [optional] 
**amount** | **Number** |  | [optional] 
**currency** | **String** |  | [optional] 
**autoComplete** | **Boolean** |  | [optional] 
**destination** | **String** |  | [optional] 



## Enum: CurrencyEnum


* `SGD` (value: `"SGD"`)

* `USD` (value: `"USD"`)




