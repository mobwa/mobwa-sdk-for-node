# Mobwa.AccountsApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createAccount**](AccountsApi.md#createAccount) | **POST** /accounts | Create an account
[**getAccount**](AccountsApi.md#getAccount) | **GET** /accounts/{accountId} | Get account information
[**listAccount**](AccountsApi.md#listAccount) | **GET** /accounts | List all accounts
[**rechargeAccount**](AccountsApi.md#rechargeAccount) | **POST** /accounts/{accountId}/recharge | Recharge the account
[**updateAccount**](AccountsApi.md#updateAccount) | **PUT** /accounts/{accountId} | Update account information
[**withdrawMoney**](AccountsApi.md#withdrawMoney) | **POST** /accounts/{accountId}/withdraw | Withdraw money from the account



## createAccount

> [Transfer] createAccount(opts)

Create an account

### Example

```javascript
import Mobwa from 'mobwa';
let defaultClient = Mobwa.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Mobwa.AccountsApi();
let opts = {
  'createAccountRequest': new Mobwa.CreateAccountRequest() // CreateAccountRequest | 
};
apiInstance.createAccount(opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **createAccountRequest** | [**CreateAccountRequest**](CreateAccountRequest.md)|  | [optional] 

### Return type

[**[Transfer]**](Transfer.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## getAccount

> Account getAccount(accountId)

Get account information

### Example

```javascript
import Mobwa from 'mobwa';
let defaultClient = Mobwa.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Mobwa.AccountsApi();
let accountId = "accountId_example"; // String | The id of the account
apiInstance.getAccount(accountId).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountId** | **String**| The id of the account | 

### Return type

[**Account**](Account.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## listAccount

> [Account] listAccount()

List all accounts

### Example

```javascript
import Mobwa from 'mobwa';
let defaultClient = Mobwa.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Mobwa.AccountsApi();
apiInstance.listAccount().then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters

This endpoint does not need any parameter.

### Return type

[**[Account]**](Account.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## rechargeAccount

> Account rechargeAccount(accountId, opts)

Recharge the account

### Example

```javascript
import Mobwa from 'mobwa';
let defaultClient = Mobwa.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Mobwa.AccountsApi();
let accountId = "accountId_example"; // String | The id of the account
let opts = {
  'rechargeAccountRequest': new Mobwa.RechargeAccountRequest() // RechargeAccountRequest | 
};
apiInstance.rechargeAccount(accountId, opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountId** | **String**| The id of the account | 
 **rechargeAccountRequest** | [**RechargeAccountRequest**](RechargeAccountRequest.md)|  | [optional] 

### Return type

[**Account**](Account.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## updateAccount

> Account updateAccount(accountId, opts)

Update account information

### Example

```javascript
import Mobwa from 'mobwa';
let defaultClient = Mobwa.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Mobwa.AccountsApi();
let accountId = "accountId_example"; // String | The id of the account
let opts = {
  'updateAccountRequest': new Mobwa.UpdateAccountRequest() // UpdateAccountRequest | 
};
apiInstance.updateAccount(accountId, opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountId** | **String**| The id of the account | 
 **updateAccountRequest** | [**UpdateAccountRequest**](UpdateAccountRequest.md)|  | [optional] 

### Return type

[**Account**](Account.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## withdrawMoney

> Account withdrawMoney(accountId, opts)

Withdraw money from the account

### Example

```javascript
import Mobwa from 'mobwa';
let defaultClient = Mobwa.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Mobwa.AccountsApi();
let accountId = "accountId_example"; // String | The id of the account
let opts = {
  'body': new Mobwa.RechargeAccountRequest() // RechargeAccountRequest | 
};
apiInstance.withdrawMoney(accountId, opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountId** | **String**| The id of the account | 
 **body** | **RechargeAccountRequest**|  | [optional] 

### Return type

[**Account**](Account.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

