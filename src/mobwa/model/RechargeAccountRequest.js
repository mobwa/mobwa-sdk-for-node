/**
 * Mobwa Payments Hub
 * Mobwa Payments Hub allows anyone to transfer money between different accounts as well as manage them. It  currently supports the following currencies: United States Dollar (USD) and Singapura Dollar (SGD) 
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The RechargeAccountRequest model module.
 * @module mobwa/model/RechargeAccountRequest
 * @version 0.20210906.0
 */
class RechargeAccountRequest {
    /**
     * Constructs a new <code>RechargeAccountRequest</code>.
     * @alias module:mobwa/model/RechargeAccountRequest
     */
    constructor() { 
        
        RechargeAccountRequest.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>RechargeAccountRequest</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:mobwa/model/RechargeAccountRequest} obj Optional instance to populate.
     * @return {module:mobwa/model/RechargeAccountRequest} The populated <code>RechargeAccountRequest</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new RechargeAccountRequest();

            if (data.hasOwnProperty('amout')) {
                obj['amout'] = ApiClient.convertToType(data['amout'], 'Number');
            }
        }
        return obj;
    }


}

/**
 * @member {Number} amout
 */
RechargeAccountRequest.prototype['amout'] = undefined;






export default RechargeAccountRequest;

