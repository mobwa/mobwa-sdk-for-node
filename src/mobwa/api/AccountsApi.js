/**
 * Mobwa Payments Hub
 * Mobwa Payments Hub allows anyone to transfer money between different accounts as well as manage them. It  currently supports the following currencies: United States Dollar (USD) and Singapura Dollar (SGD) 
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */


import ApiClient from "../ApiClient";
import Account from '../model/Account';
import CreateAccountRequest from '../model/CreateAccountRequest';
import Error from '../model/Error';
import RechargeAccountRequest from '../model/RechargeAccountRequest';
import Transfer from '../model/Transfer';
import UpdateAccountRequest from '../model/UpdateAccountRequest';

/**
* Accounts service.
* @module mobwa/api/AccountsApi
* @version 0.20210906.0
*/
export default class AccountsApi {

    /**
    * Constructs a new AccountsApi. 
    * @alias module:mobwa/api/AccountsApi
    * @class
    * @param {module:mobwa/ApiClient} [apiClient] Optional API client implementation to use,
    * default to {@link module:mobwa/ApiClient#instance} if unspecified.
    */
    constructor(apiClient) {
        this.apiClient = apiClient || ApiClient.instance;
    }



    /**
     * Create an account
     * @param {Object} opts Optional parameters
     * @param {module:mobwa/model/CreateAccountRequest} opts.createAccountRequest 
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing data of type {@link Array.<module:mobwa/model/Transfer>} and HTTP response
     */
    createAccountWithHttpInfo(opts) {
      opts = opts || {};
      let postBody = opts['createAccountRequest'];

      let pathParams = {
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['basicAuth'];
      let contentTypes = ['application/json'];
      let accepts = ['application/json'];
      let returnType = [Transfer];
      return this.apiClient.callApi(
        '/accounts', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null
      );
    }

    /**
     * Create an account
     * @param {Object} opts Optional parameters
     * @param {module:mobwa/model/CreateAccountRequest} opts.createAccountRequest 
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link Array.<module:mobwa/model/Transfer>}
     */
    createAccount(opts) {
      return this.createAccountWithHttpInfo(opts)
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }


    /**
     * Get account information
     * @param {String} accountId The id of the account
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing data of type {@link module:mobwa/model/Account} and HTTP response
     */
    getAccountWithHttpInfo(accountId) {
      let postBody = null;
      // verify the required parameter 'accountId' is set
      if (accountId === undefined || accountId === null) {
        throw new Error("Missing the required parameter 'accountId' when calling getAccount");
      }

      let pathParams = {
        'accountId': accountId
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['basicAuth'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = Account;
      return this.apiClient.callApi(
        '/accounts/{accountId}', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null
      );
    }

    /**
     * Get account information
     * @param {String} accountId The id of the account
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:mobwa/model/Account}
     */
    getAccount(accountId) {
      return this.getAccountWithHttpInfo(accountId)
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }


    /**
     * List all accounts
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing data of type {@link Array.<module:mobwa/model/Account>} and HTTP response
     */
    listAccountWithHttpInfo() {
      let postBody = null;

      let pathParams = {
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['basicAuth'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = [Account];
      return this.apiClient.callApi(
        '/accounts', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null
      );
    }

    /**
     * List all accounts
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link Array.<module:mobwa/model/Account>}
     */
    listAccount() {
      return this.listAccountWithHttpInfo()
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }


    /**
     * Recharge the account
     * @param {String} accountId The id of the account
     * @param {Object} opts Optional parameters
     * @param {module:mobwa/model/RechargeAccountRequest} opts.rechargeAccountRequest 
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing data of type {@link module:mobwa/model/Account} and HTTP response
     */
    rechargeAccountWithHttpInfo(accountId, opts) {
      opts = opts || {};
      let postBody = opts['rechargeAccountRequest'];
      // verify the required parameter 'accountId' is set
      if (accountId === undefined || accountId === null) {
        throw new Error("Missing the required parameter 'accountId' when calling rechargeAccount");
      }

      let pathParams = {
        'accountId': accountId
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['basicAuth'];
      let contentTypes = ['application/json'];
      let accepts = ['application/json'];
      let returnType = Account;
      return this.apiClient.callApi(
        '/accounts/{accountId}/recharge', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null
      );
    }

    /**
     * Recharge the account
     * @param {String} accountId The id of the account
     * @param {Object} opts Optional parameters
     * @param {module:mobwa/model/RechargeAccountRequest} opts.rechargeAccountRequest 
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:mobwa/model/Account}
     */
    rechargeAccount(accountId, opts) {
      return this.rechargeAccountWithHttpInfo(accountId, opts)
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }


    /**
     * Update account information
     * @param {String} accountId The id of the account
     * @param {Object} opts Optional parameters
     * @param {module:mobwa/model/UpdateAccountRequest} opts.updateAccountRequest 
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing data of type {@link module:mobwa/model/Account} and HTTP response
     */
    updateAccountWithHttpInfo(accountId, opts) {
      opts = opts || {};
      let postBody = opts['updateAccountRequest'];
      // verify the required parameter 'accountId' is set
      if (accountId === undefined || accountId === null) {
        throw new Error("Missing the required parameter 'accountId' when calling updateAccount");
      }

      let pathParams = {
        'accountId': accountId
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['basicAuth'];
      let contentTypes = ['application/json'];
      let accepts = ['application/json'];
      let returnType = Account;
      return this.apiClient.callApi(
        '/accounts/{accountId}', 'PUT',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null
      );
    }

    /**
     * Update account information
     * @param {String} accountId The id of the account
     * @param {Object} opts Optional parameters
     * @param {module:mobwa/model/UpdateAccountRequest} opts.updateAccountRequest 
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:mobwa/model/Account}
     */
    updateAccount(accountId, opts) {
      return this.updateAccountWithHttpInfo(accountId, opts)
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }


    /**
     * Withdraw money from the account
     * @param {String} accountId The id of the account
     * @param {Object} opts Optional parameters
     * @param {module:mobwa/model/RechargeAccountRequest} opts.body 
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing data of type {@link module:mobwa/model/Account} and HTTP response
     */
    withdrawMoneyWithHttpInfo(accountId, opts) {
      opts = opts || {};
      let postBody = opts['body'];
      // verify the required parameter 'accountId' is set
      if (accountId === undefined || accountId === null) {
        throw new Error("Missing the required parameter 'accountId' when calling withdrawMoney");
      }

      let pathParams = {
        'accountId': accountId
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['basicAuth'];
      let contentTypes = ['application/json'];
      let accepts = ['application/json'];
      let returnType = Account;
      return this.apiClient.callApi(
        '/accounts/{accountId}/withdraw', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null
      );
    }

    /**
     * Withdraw money from the account
     * @param {String} accountId The id of the account
     * @param {Object} opts Optional parameters
     * @param {module:mobwa/model/RechargeAccountRequest} opts.body 
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:mobwa/model/Account}
     */
    withdrawMoney(accountId, opts) {
      return this.withdrawMoneyWithHttpInfo(accountId, opts)
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }


}
