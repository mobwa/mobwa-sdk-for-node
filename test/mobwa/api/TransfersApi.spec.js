/**
 * Mobwa Payments Hub
 * Mobwa Payments Hub allows anyone to transfer money between different accounts as well as manage them. It  currently supports the following currencies: United States Dollar (USD) and Singapura Dollar (SGD) 
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', process.cwd()+'/src/mobwa/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require(process.cwd()+'/src/mobwa/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.Mobwa);
  }
}(this, function(expect, Mobwa) {
  'use strict';

  var instance;

  beforeEach(function() {
    instance = new Mobwa.TransfersApi();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('TransfersApi', function() {
    describe('completeTransfer', function() {
      it('should call completeTransfer successfully', function(done) {
        //uncomment below and update the code to test completeTransfer
        //instance.completeTransfer(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('createTransfer', function() {
      it('should call createTransfer successfully', function(done) {
        //uncomment below and update the code to test createTransfer
        //instance.createTransfer(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('deleteTransfer', function() {
      it('should call deleteTransfer successfully', function(done) {
        //uncomment below and update the code to test deleteTransfer
        //instance.deleteTransfer(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('getTransfer', function() {
      it('should call getTransfer successfully', function(done) {
        //uncomment below and update the code to test getTransfer
        //instance.getTransfer(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('listTransfers', function() {
      it('should call listTransfers successfully', function(done) {
        //uncomment below and update the code to test listTransfers
        //instance.listTransfers(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('updateTransfer', function() {
      it('should call updateTransfer successfully', function(done) {
        //uncomment below and update the code to test updateTransfer
        //instance.updateTransfer(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
  });

}));
