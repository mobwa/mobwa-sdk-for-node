# Mobwa.CreateUserRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**username** | **String** |  | [optional] 
**email** | **String** |  | [optional] 
**password** | **String** |  | [optional] 
**role** | **String** |  | [optional] 



## Enum: RoleEnum


* `CUSTOMER` (value: `"CUSTOMER"`)

* `ADMIN` (value: `"ADMIN"`)




