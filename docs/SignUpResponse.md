# Mobwa.SignUpResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**username** | **String** |  | [optional] 
**email** | **String** |  | [optional] 
**role** | **String** |  | [optional] 
**accounts** | [**[Account]**](Account.md) |  | [optional] 



## Enum: RoleEnum


* `customer` (value: `"customer"`)

* `admin` (value: `"admin"`)




